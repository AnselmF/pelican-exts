===========================
Anselm's Pelican Extensions
===========================

This is a roll-up repo of the extensions for my blog at https://blog.tfiu.de
I have accumulated over the time.  Actual documentation (for what it's
worth) is in blog posts linked to in the various extensions.  Here is a
quick overview over the various files with links to the respective posts.

Pelican plugins
---------------

* plugins/articlemtime.py – changes the mtime on articles produced to
  what their sources are.  That's nice for harvesters in general, but
  in particular for the local search engine (see blogsearch below). See
  http://blog.tfiu.de/how-i-m-using-pelican.html.  Also note that if
  you use this, you probably want ``DELETE_OUTPUT_DIRECTORY = False``
  in publishconf.py (which is not the default).

* plugins/citedby.py – adds (somewhat haphazardly) links from posts
  to later posts that cite them.  See
  http://blog.tfiu.de/how-i-m-using-pelican.html.

* plugins/displaypull.py – adds a ReStructuredText directive to have
  decorative pulled quotes when you use pull-quote for actual
  quotations.  Perhaps that's not a good idea, and there's no post about
  that.

* plugins/rstfeedback.py – adds ReStructuredText directives ``feedback`` for
  marking up user feedback and ``addition`` for later author additions.
  You also get a list of such items for template inclusions.  See
  http://blog.tfiu.de/feedback-and-addenda-in-pelican-posts.html.

* plugins/rstoriginal.py – adds a ReStructuredText directive ``original`` to
  mark up initially hidden original text when there's a translation of a
  citation.  Also doesn't have a post yet, but you can see what it's
  intended to be used like on http://blog.tfiu.de/ein-bekanntes-szenario.html

* plugins/taginfo.py – lets you add explanatory text to your tags pages.
  Explained in http://blog.tfiu.de/explaining-tags-in-pelican.html

* plugins/ditdate.py – adds an attribute ``.starDIT`` to article objects
  that you can then use to show the article's timestamp in DIT_.
  `Here's advocacy`_ for why you may want to do that.

.. _Here's advocacy: https://blog.tfiu.de/saner-timestamps-with-dit-in-pelican-and-beyond.html
.. _DIT: https://djeekay.net/dit/


Server-side stuff
-----------------

* cgi/blogsearch – a xapian-based local search engine, cgi and indexer.
  Discussed in
  http://blog.tfiu.de/a-local-search-engine-for-pelican-based-blogs.html
  and http://blog.tfiu.de/stemming-for-the-search-engine.html.
* cgi/feedback – a server-side component that lets people send in comments
  using a convenient text box next to the article itself.  Discussed
  in http://blog.tfiu.de/a-feedback-form-in-pelican.html.
* cgi/cgiserver.py – when your web server can't do CGI, you can use
  this beast.  I really need to write a post on this.

Where not stated otherwise, all this code is distributed under CC0.
