"""
taginfo
=============================

This plugin adds description blocks to tag pages

To use this, have a taginfo subdirectory in your pelican directory, and in
there <tagname>.rst[x] files giving the description of the tag in
reStructuredText.  Then, in the tags template, but something like::

	<div id="taginfo">
		{{ tag.make_description() }}
	</div>
"""

import io
import os

from docutils import core as rstcore

from pelican import signals
from pelican.urlwrappers import Tag


def rstxToHTMLWithWarning(source, **userOverrides):
	"""returns HTML and a string with warnings for a piece of ReStructured
	text.

	source can be a unicode string or a byte string in utf-8.

	userOverrides will be added to the overrides argument of docutils'
	core.publish_parts.
	"""
	sourcePath, destinationPath = None, None
	
	warnAccum = io.StringIO()
	overrides = {'input_encoding': 'unicode',
		'raw_enabled': True,
		'doctitle_xform': None,
		'warning_stream': warnAccum,
		'initial_header_level': 4}
	overrides.update(userOverrides)

	parts = rstcore.publish_parts(
		source=source+"\n", source_path=sourcePath,
		destination_path=destinationPath,
		writer_name='html', settings_overrides=overrides)
	return parts["fragment"], warnAccum.getvalue()



def make_description(self):
    """returns a description to insert into tag info pages.

    This is generated from reStructuredText in RST files.
    """
    src = None
    for ext in [".rst", ".rstx"]:
        try:
            with open(os.path.join(
                    make_description.desc_path,
                    self.name+ext), "r", encoding="utf-8") as f:
                src = f.read()
                break
        except IOError:
            pass

    if src:
        return rstxToHTMLWithWarning(src)[0]

    return ""


def _monkeypatch_tag_object(pelican):
    make_description.desc_path = os.path.join(
        pelican.settings["PATH"],
        "..",
        "taginfo")
    Tag.make_description = make_description


def register():
    signals.initialized.connect(_monkeypatch_tag_object)

# vim:sta:et:sw=4
