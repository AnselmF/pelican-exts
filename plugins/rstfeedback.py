"""
rstfeedback
=============================

This plugin adds two ReStructuredText directives, ``feedback`` for marking
up replies, and ``addition`` for marking up later additions to articles
(usually by the author).

There's a blog post on this at
http://blog.tfiu.de/feedback-and-addenda-in-pelican-posts.html

The comments are numbered and id-ed <class>-<running number>; these ids
regrettably are not stable when you add items of the same class above them
(they are assigned in lexical order).  It's not easy to fix that with
automatic id assignment, so let's see how bad that turns out to be.

An example for a feedback item::

    .. feedback::
      :author: someone or other
      :date: 2022-03-07

      Example, yadda.


If your date isn't an ISO string, the comment won't be added into the
comment database.

This nests reasonably nicely with CSS like::

    div.feedback {
        width: 95%;
        margin: 1rem 0pt 0.5rem auto;
        border-radius: 6pt 0pt 6pt 0pt;
        border: 1pt solid #335500;

    }

    .feedback-header {
        margin: 0pt;
        background: #335500;
        color: white;
        padding: 3pt 4pt 4pt 4pt;
        border-radius: 6pt 0pt 0pt 0pt;
    }

    .feedback p {
        padding: 4pt;
    }


Additions work like this::

    .. addition::
      :date: 2022-03-07

      Example, yadda.

These will currently break messily without a date.  CSS for that could be::

    div.addition {
	    border-left: 2pt solid #ccc;
	    padding-left: 4pt;
    }

    .addition-header {
	    font-weight: bold;
    }

This also adds a template variable LAST_ADDITIONS containing the
last 10 addtions items seen.  This is a list of objects having attributes
url, title, count, kind, and date, sorted by date descending.  You could format
those with something like::

    	<h2>Letzte Ergänzungen</h2>
			<ul class="feedback">
			{% for feedback_item in LAST_FEEDBACK_ITEMS %}
				<li><a href="{{ SITEURL }}/{{ feedback_item.url }}">{{ feedback_item.kind }} zu „{{ feedback_item.title }}“</a> ({{ feedback_item.date }})</li>
			{% endfor %}
			</ul>

We keep a sqlite database of all comments in the content directory; this is
mainly for the benefit of cached runs.

Localisation is at this point by changing LABEL_FOR_CLASS inline.  Ahem.

Distributed under CC-0
"""

import datetime
import os
import sqlite3 as sqlite

from docutils import nodes
from docutils.parsers.rst import directives
from docutils.parsers.rst.directives import body

from pelican import signals
from pelican import utils


# title labels per supplement class
LABEL_FOR_CLASS = {
    "feedback": "Kommentar",
    "addition": "Nachtrag",
}


def get_slug(root):
    """returns the slug of a pelican article.

    It is searched in a field_list child of root.  If it is not found
    a KeyError for slug is raised.
    """
    for child in root:
        if child.tagname=="field_list":
            for field in child:
                if field[0].astext()=="slug":
                    return field[1].astext()
    raise KeyError("slug")


def get_title(root):
    """returns the title of a pelican article.

    That's the first title child for the root element.  If there is no
    such element, a KeyError for title is raised.
    """
    for child in root:
        if child.tagname=="title":
            return child.astext()
    raise KeyError("title")


class SupplementDirective(body.BlockQuote):
    """A directive to include later additions into blog posts.

    These have two RST options, date (must be a Y-m-d ISO string) and author
    (optional).

    Derivations need to fill in one attribute

    * css_class (the css class of the resulting div, also used for a
      few other purposes)

    A css class must also be added in the LABEL_FOR_CLASS dictionary
    (that needs to be localised; ah well), where its value is what's
    added in the RST header lines

    Derivations must also define one method

    * get_header(id, author, date) -> [docutils material]

    returning a suitable header for the addition.  Note that both author
    and date may be None.

    The sql table must be created externally (here: in setup_db_connection),
    the DB connection is taken from the global DB_CONNECTION.
    """
    option_spec = {
        "author": directives.unchanged,
        "date": directives.unchanged,  # must be Y-m-d ISO
    }

    def __init__(self, *args, **kwargs):
        self.classes = [self.css_class]
        self.counter_name = f"_rfb_{self.css_class}_count"
        super().__init__(*args, **kwargs)

    def _get_count(self):
        """returns a per-document count of the current sort of supplement.

        We keep a comment count in the state machine in hopes that that'll
        be reset by-document.
        """
        return getattr(self.state.document, self.counter_name, 1)

    def _set_count(self, val):
        """sets the per-document supplement count.
        """
        setattr(self.state.document, self.counter_name, val)

    count = property(_get_count, _set_count)

    def run(self):
        comment_id = self.count

        date = self.options.get("date")
        if date:
            try:
                date = datetime.datetime.strptime(
                    date, "%Y-%m-%d").date()
            except ValueError:
                # malformed date; clear silently for now
                date = None

        html_id = f"{self.css_class}-{comment_id}"
        result = nodes.container(
            self.content,
            nodes.paragraph("", "",
                *self.get_header(
                    html_id,
                    self.options.get("author"),
                    date),
                classes=[f"{self.css_class}-header"]),
            classes=[self.css_class],
            ids=[html_id])
        self.count = self.count+1

        self.state.nested_parse(self.content, self.content_offset, result)

        if date:
            title = get_title(self.state.document.children[0])
            try:
                slug = get_slug(self.state.document.children[0])
            except KeyError:
                slug = utils.slugify(title)

            DB_CONNECTION.execute("INSERT INTO supplements"
                " (date, slug, title, kind, identifier)"
                " VALUES (?, ?, ?, ?, ?)"
                " ON CONFLICT DO NOTHING",
                (date, slug, title,
                    LABEL_FOR_CLASS[self.css_class], html_id))

        return [result]


class FeedbackDirective(SupplementDirective):
    css_class = "feedback"

    def get_header(self, id, author, date):
        id = id.split("-", 1)[-1]
        children = [nodes.Text("{} {}".format(
            LABEL_FOR_CLASS[self.css_class], id))]

        if date:
            children.append(
                nodes.inline(date, " am "+date.strftime("%Y-%m-%d"),
                    classes=[f"{self.css_class}-date"]))

        if author:
            children.append(
                nodes.inline(author,
                    " von "+author,
                    classes=[f"{self.css_class}-author"]))

        return children


class AdditionDirective(SupplementDirective):
    css_class = "addition"

    def get_header(self, id, author, date):
        return [nodes.Text("{} ({})".format(
            LABEL_FOR_CLASS[self.css_class], date.strftime("%Y-%m-%d")))]

_DDL = ["""CREATE TABLE IF NOT EXISTS supplements (
    date DATE,
    slug TEXT,
    title TEXT,
    kind TEXT,
    identifier TEXT)""",
    "CREATE UNIQUE INDEX IF NOT EXISTS supplements_primary ON supplements"
    " (slug, identifier)"]


def _setup_db_connection(pelican):
    global DB_CONNECTION
    db_path = os.path.join(pelican.settings["PATH"], ".replies.db")
    DB_CONNECTION = sqlite.connect(
        db_path, isolation_level=None)
    for statement in _DDL:
        DB_CONNECTION.execute(statement)


class _FeedbackItem:
    def __init__(self, date, slug, title, kind, identifier):
        self.date = date
        self.url = f"{slug}.html#{identifier}"
        self.title = title
        self.kind = kind
        self.count = identifier.split("-", 1)[-1]


def _set_variables(generators):
    """adds the LAST_FEEDBACK_ITEMS variable to the page context.
    """
    if not generators:
        return

    cursor = DB_CONNECTION.cursor()
    cursor.execute("SELECT date, slug, title, kind, identifier FROM supplements"
        " ORDER BY date DESC LIMIT 10")
    generators[0].context["LAST_FEEDBACK_ITEMS"] = [
        _FeedbackItem(*row) for row in cursor]


def register():
    signals.readers_init.connect(_setup_db_connection)
    signals.all_generators_finalized.connect(_set_variables)
    directives.register_directive("feedback", FeedbackDirective)
    directives.register_directive("addition", AdditionDirective)

# vim:sta:et:sw=4
