"""
displaypull
=============================

This plugin adds a ReStructuredText directive ``display-pull``.  This
is a bit as RST's pull-quote, but intended for short snippets intended
to break up endless text.

This is intended to be used with css like, perhaps::

	blockquote.display-pull {
		text-align: right;
		font-style: italic;
		margin-left: 30%;
		margin-right: 0px;
		padding: 0.5rem;
		background-color: rgba(255,255,255,0.2);
		border-radius: 0.5rem;
	}

Distributed under CC-0
"""

from docutils import nodes
from docutils.parsers.rst import directives
from docutils.parsers.rst.directives import body


class DisplayPullDirective(body.BlockQuote):
	classes = ["display-pull"]


def register():
	directives.register_directive("display-pull", DisplayPullDirective)
