"""
articlemtime
=============================

This plugin manages the mtime of written articles to match the mtimes
of their sources.
"""

import os


from pelican import signals


def set_written_mtime(generator, writer):
	"""copies atime and mtime for the items written by generator
	from the source to the target files.

	This is supposed to work for both articles and pages.

	We don't support static files yet.
	"""
	items = getattr(generator, "articles", None) or generator.pages
	for article in items:
		dest_path = os.path.join(generator.output_path, article.save_as)
		os.utime(dest_path, (
			os.path.getatime(article.source_path),
			os.path.getmtime(article.source_path)))


def register():
	signals.article_writer_finalized.connect(set_written_mtime)
	signals.page_writer_finalized.connect(set_written_mtime)
