"""
citedby
=============================

This plugin lets you add links to articles when they you cite them.

Dependencies: BeautifulSoup 4, lxml.

To use this, link as usual and then, you your template, say something
like::

    {% if article.citedby %}
      <p class="citedby">Zitiert in:
      {% for c in article.citedby %}
          <span class="citedlink"><a href="{{ c.href }}">
              {{ c.anchor }}</a></span>
      {% endfor %}
      </p>
    {% endif %}

We keep the citing-cited pairs in a SQLite database in content/.  Since
we do a single pass, a second generation run is necessary to actually
insert the new citations.  To make this possible in installations
that do incremental updates, the plugin creates a file __REMAKE__ containing
the URLs that received previously unseen citations in the content folder.

The idea is that on the next run, the machinery can pick this up to force
re-creation of just those files.  I'll look into it when I move my site
to incremental updates.
"""

import contextlib
import logging
import os
import sqlite3 as sqlite

from bs4 import BeautifulSoup
from pelican import signals

logger = logging.getLogger(__name__)

CITATION_GRAPH = None


# we de-normalise a bit in that we store the citing title in every
# row -- but that's harmless because we tear down on citing before
# inserting, and it's long like we'll have 100s of citations from a
# single article.
_DDL = ["""CREATE TABLE IF NOT EXISTS citations (
    cited TEXT,
    citing TEXT,
    citing_title TEXT)""",
    "CREATE INDEX IF NOT EXISTS ed_index ON citations (cited)",
    "CREATE INDEX IF NOT EXISTS ing_index ON citations (citing)",]


def iter_internal_citations(content):
    """iterates over the internal citation in content.
    """
    sentinel = "$$INTERNAL$$"
    soup = BeautifulSoup(content.get_content(sentinel), "lxml")
    for el in soup.find_all("a"):
        dest = el.get("href")
        if dest and dest.startswith(sentinel):
            yield dest[len(sentinel)+1:].split("#")[0]


class CitationGraph:
    """The cited-citing graph, kept in a sqlite DB.

    This is intended as a singleton, with the instance created in
    make_citation_graph and kept in the global CITATION_GRAPH; making
    more of these should work just fine, though (except that you must
    make sure to always use the same work_dir, or you'll get multiple
    disjoint graphs).
    """
    def __init__(self, work_dir):
        self.work_dir = work_dir
        db_path = os.path.join(self.work_dir, ".citedby.db")
        self.conn = sqlite.connect(db_path)
        self._ensure_schema()
        self.to_be_remade = set()

    @contextlib.contextmanager
    def session(self):
        """manages a cursor on our connection.

        This will autocommit unless an exception is raised, in which
        case the connection will be rolled back.

        I'm not defining anything as to what happens if you take out
        more of these at a time.
        """
        cursor = self.conn.cursor()
        try:
            yield cursor
            self.conn.commit()
        except:
            self.conn.rollback()
            raise

    def _add_citedby(self, article_generator):
        """adds the citedby metadata items to article_generator.

        This only makes sense as a callback for article_generator_finalized.
        """
        for article in article_generator.articles:
            cb = [{"href": "/"+link, "anchor": title}
                for link, title in self.get_citing_for(article.url)]
            if cb:
                article.citedby = cb
        article.irgendwas = "irgendwas"

    def _save_new_citeds(self, page_generator, writer):
        """saves the urls of the pages that got new citations this
        time into a file work_dir/__REMAKE__.
        """
        dest_file = os.path.join(self.work_dir, "__REMAKE__")
        if self.to_be_remade:
            with open(dest_file, "w", encoding="utf-8") as f:
                f.write("\n".join(self.to_be_remade)+"\n")
        else:
            if os.path.exists(dest_file):
                os.unlink(dest_file)

    def _ensure_schema(self):
        """creates our table if necessary.

        Actually, we just blindly execute _DDL, trusting it's been
        written to be idempotent.
        """
        with self.session() as sess:
            for statement in _DDL:
                sess.execute(statement)

    def add_pairs(self, citing, citing_title, list_of_cited):
        """adds pairs of (citing, cited) into the database.

        Any previous references of citing are dropped.  Hence, to
        remove citing from the database, just pass an empty list_of_cited.

        This will also feed any new cited targets to our new_cited
        attribute; these are articles that would need to become re-made
        once we support partial makes our automate the double make.
        """
        with self.session() as sess:
            cited_so_far = set(r[0] for r in sess.execute(
                "SELECT cited FROM citations"
                " WHERE citing=?", (citing,)))

            sess.execute("DELETE FROM citations WHERE citing=?",
                (citing,))
            sess.executemany("INSERT INTO citations"
                " (citing, citing_title, cited)"
                " VALUES (?, ?, ?)",
                [(citing, citing_title, cited) for cited in list_of_cited])

            cited_now = set(r[0] for r in sess.execute(
                "SELECT cited FROM citations"
                " WHERE citing=?", (citing,)))

            self.to_be_remade.update(cited_now-cited_so_far)

    def get_citing_for(self, cited):
        """returns a list of articles citing cited as url, title pairs.
        """
        with self.session() as sess:
            return list(
                sess.execute("SELECT DISTINCT citing, citing_title"
                    " FROM citations"
                    " WHERE cited=?", (cited,)))

    def add_from_pelican(self, article_generator, content):
        """adds internal links in content to our database.
        """
        articles_cited = set(iter_internal_citations(content))
        if articles_cited:
            self.add_pairs(content.url, content.title, articles_cited)


def make_citation_graph(pelican):
    """returns the citation graph singleton, creating it if necessary.
    """
    global CITATION_GRAPH

    if CITATION_GRAPH:
        return CITATION_GRAPH

    CITATION_GRAPH = CitationGraph(pelican.settings["PATH"])
    signals.article_generator_write_article.connect(
        CITATION_GRAPH.add_from_pelican)
    signals.article_generator_finalized.connect(
        CITATION_GRAPH._add_citedby)
    signals.page_writer_finalized.connect(
        CITATION_GRAPH._save_new_citeds)


def register():
    signals.initialized.connect(make_citation_graph)


if __name__=="__main__":
    # Code for playing around; assumes there's a directory citedby-test
    # in the user's home.
    cg = CitationGraph(os.path.expanduser("~/citedby-test"))
    cg.add_pairs("art1", ["artA", "artB"])
    cg.add_pairs("art2", ["artB", "artC"])
    assert cg.get_citing_for("artA")==["art1"]
    assert set(cg.get_citing_for("artB"))=={"art2", "art1"}
    print("looking good.")

# vim:sta:et:sw=4
