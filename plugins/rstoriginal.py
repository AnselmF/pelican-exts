"""
rstoriginal
=============================

This plugin adds a ReStructuredText directive ``original`` for marking
up the original version, typically after a pull-quote.

This is intended to be used with css like this::

	.original-container * {
		display: none;
	}

	.original-header:hover ~ * {
		display: block;
		padding: 0.2ex 2pt 0.2ex 4em;
		margin-top: 0;
		margin-right: 0;
		margin-left: 5%;
		background-color: #FFFFFF;
		background-image:url(/theme/image/quotation.png);
		background-position: top left;
		background-repeat: no-repeat;
		font-size: 92%;
		text-align: right;
	}

	.original-header:hover {
		font-weight: bold;
		font-size: 120%;
		background-color: #FFFFFF;
		margin-left: 5%;
		margin-bottom: 0;
	}

	.original-container .original-header {
		display:block;
		text-align: right;
		font-style: italic;
  	color: #414abe;
	}

Distributed under CC-0
"""

from docutils import nodes
from docutils.parsers.rst import directives
from docutils.parsers.rst.directives import body


class OriginalDirective(body.BlockQuote):
	classes = ["original"]

	def run(self):
		original_text = super().run()
		return [nodes.sidebar(self.content,
				nodes.paragraph("", "",
					nodes.Text("Original"), classes=["original-header"]),
				*original_text,
				classes=["original-container"])]


def register():
	directives.register_directive("original", OriginalDirective)
