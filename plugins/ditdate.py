"""
ditdate
=============================

This pelican plugin adds a starDIT attribute to your posts containing
th starDIT of its post date; see https://djeekay.net/dit/.

I'm using it in the article template like this:

        <a href="https://djeekay.net/dit/">DIT</a>
        <abbr class="StarDIT">
        	{{ article.starDIT[:-4] }}
        </abbr>
        (<abbr class="date">{{ article.date.strftime("%Y-%m-%d") }}</abbr>)

No non-stdlib dependencies.
"""

import datetime

from pelican import signals


DIT_ZONE = datetime.timezone(datetime.timedelta(hours=-12))
UTC = datetime.timezone.utc


def get_dit(dt=None):
	"""returns strings for the StarDIT for a datetime instance (or now, with
	empty arguments).

	Naive datetimes are interpreted as local time; hence, the second
	doctest will fail for you if you're not in CET.  I'll change it
	when others use this.  At this point, we always round down.

	>>> get_dit(datetime.datetime(2023, 12, 10, 12, 00, tzinfo=UTC))
	('12023:344', '0.00.00')
	>>> get_dit(datetime.datetime(2023, 12, 10, 12, 00))
	('12023:343', '9.58.33')
	>>> get_dit(datetime.datetime(2023, 1, 1, 9, 21, tzinfo=UTC))
	('12022:365', '8.89.58')
	"""
	if dt is None:
		dt = datetime.datetime.now(UTC)
	dit_stamp = dt.astimezone(DIT_ZONE)

	dit_time = dit_stamp.time()
	dit_digits = '{:05d}'.format(
		int((dit_time.hour*3600+dit_time.minute*60+dit_time.second)/0.86400))
	dit_str = dit_digits[0]+"."+dit_digits[1:3]+"."+dit_digits[3:5]

	date_str = "{:d}:{:03d}".format(
		dit_stamp.year+10000,
		dit_stamp.timetuple()[7])

	return date_str, dit_str


def add_dit(pelican, content):
	if hasattr(content, "date"):
		content.starDIT = ":".join(get_dit(content.date))


def register():
		signals.article_generator_write_article.connect(add_dit)
