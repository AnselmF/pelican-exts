#!/usr/bin/python3
# This is a basic CGI executor using python's built-in cgi support.
# You pass it the the port to listen on and the directory the CGIs are 
# in; whatever is executable in there this thing will try to run as a CGI.
# Oh: we reject any non-ASCII paths.
#
# One rather massive drawback: the SCRIPT_NAME environment variable is
# wrong due since we separate on-disk paths and logical URI paths.
# We'd have to hack rather deeply into http.server to fix this.
# I know it's a pain...
#
# The recommended deployment is through nginx:
#
#  location /bin/ {
#    proxy_pass http://localhost:6070;
#    proxy_set_header Host $host;
#  }
# 
# In apache (where this doesn't make a lot of sense),
# you'd say   ProxyPass /bin/ http://localhost:6070/   or so.
#
# See below for a sysvinit script.


import io
import os
import subprocess
from http import HTTPStatus
from http import server


class CGIRunner(server.CGIHTTPRequestHandler):

	# Extra defense in case http.server implementation changes (CGI
	# determination should then crash)
	cgi_directories = None

	def translate_path(self, path):
		# we insist on only serving things exactly from our cgi_directory;
		# cgi_directory is guaranteed to not end with a slash, path
		# always has a leading slash
		return self.server.cgi_directory+path

	def is_cgi(self):
		# here, we inspect all parts of the path and are happy
		# once we find that name in our CGI directory.  If
		# we don't, we bail out immediately, as we don't serve 
		# plain files here.

		# Some extra protection against lame tricks
		cleaned_path = self.path.encode("ascii").decode("ascii")

		with_query = cleaned_path.split("?")
		path_parts = with_query[0].split("/")
		for index, name in enumerate(path_parts):
			script_path = os.path.join(self.server.cgi_directory, name)

			if os.path.isfile(script_path) and os.access(script_path, os.X_OK):
				with_query[0] = "/".join(path_parts[index:])
				self.cgi_info = (
					"/".join(path_parts[:index-1]),
					"?".join(with_query))
				break
		else:
			# don't serve normal files; this will bomb out in
			# handle_one_request
			self.cgi_info = None

		# never serve anything but a CGI
		return True

	def run_cgi(self):
		if self.cgi_info is None:
			self.send_error(
				HTTPStatus.FORBIDDEN,
				"Will only serve the result of a CGI execution")
			return

		if self.server.debug:
			self.have_fork = False
			orig_wfile = self.wfile
			self.wfile = io.BytesIO()

		res = server.CGIHTTPRequestHandler.run_cgi(self)

		if self.server.debug:
			orig_wfile.write(self.wfile.getvalue())
			print(self.wfile.getvalue())


class CGIServer(server.ThreadingHTTPServer):
	"""An http server that only executes CGIs from a single directory
	and rejects all other requests.
	"""
	def __init__(self, server_address, cgi_directory, debug):
		self.cgi_directory = cgi_directory
		self.debug = debug
		server.ThreadingHTTPServer.__init__(self, server_address, CGIRunner)

	def log_message(self, format, *args):
		pass


def parse_command_line():
	import argparse
	parser = argparse.ArgumentParser(description="A CGI executor")
	parser.add_argument("-d", "--debug", action="store_true",
		help="Dump script I/O to stdout")
	parser.add_argument("port", type=int, 
		help="Port (on localhost) to listen on")
	parser.add_argument("path", type=str,
		help="Path to where the CGIs are (only executable files in exactly"
			" that directory are executed, all other requests are rejected).")
	return parser.parse_args()


class DebugPopen(subprocess.Popen):
	def __init__(self, *args, **kwargs):
		print(kwargs["env"])
		super().__init__(*args, **kwargs)


def main():
	args = parse_command_line()

	if args.debug:
		subprocess.Popen = DebugPopen

	srv = CGIServer(
		("localhost", args.port), 
		args.path.rstrip("/"),
		args.debug)
	srv.serve_forever()


if __name__=="__main__":
	main()


# Here's a sysvinit script to start this:
"""
#!/bin/sh
### BEGIN INIT INFO
# Provides:          cgiserver
# Required-Start:    $local_fs $network
# Required-Stop:     $local_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: CGI executor
# Description:       An http server executing CGI scripts in $CGIS
### END INIT INFO

# custom enviroment
# export anything you want to communicate to the CGIs here.


# where are the CGIs?
CGIS=/var/www/blog-media/cgi
# What port to listen to?
PORT=6070
# where to log to?
LOGFILE=/var/log/cgiserver.log 

SCRIPT_LOCATION=/var/www/blog-media/cgiserver.py
DAEMONOPTS="--pidfile /run/cgiserver.pid --oknodo --exec /usr/bin/python3"

do_start() {
		start-stop-daemon --start $DAEMONOPTS \
			--make-pidfile --background --chuid nobody \
			--output "$LOGFILE" \
			-- "$SCRIPT_LOCATION" $PORT "$CGIS"
}

do_stop() {
		start-stop-daemon --stop $DAEMONOPTS \
			--remove-pidfile \
			-- "$SCRIPT_LOCATION" || exit "Stop failed: $?"
}


case "$1" in
	start)
		do_start
		;;
	stop)
		do_stop
		;;
	restart)
		do_stop
		do_start
		;;
	*)
		echo "USAGE: $0 start|stop|restart"
		;;
esac

exit 0
"""
